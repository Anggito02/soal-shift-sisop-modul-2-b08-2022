# soal-shift-sisop-modul-2-B08-2022

## Daftar Isi

- [Anggota Kelompok](#anggota-kelompok)
- [Nomor 1](#nomor-1)
    - Deskripsi Soal
    - [Kendala Nomor 1](#kendala-nomor-1)
    - [Solusi Nomor 1](#solusi-nomor-1)
    - [Source Code Nomor 1](https://gitlab.com/Anggito02/soal-shift-sisop-modul-2-b08-2022/-/tree/main/soal1)
- [Nomor 2](#nomor-2)
    - Deskripsi Soal
    - [Kendala Nomor 2](#kendala-nomor-2)
    - [Solusi Nomor 2](#solusi-nomor-2)
    - [Source Code Nomor 2](https://gitlab.com/Anggito02/soal-shift-sisop-modul-2-b08-2022/-/tree/main/soal2)
- [Nomor 3](#nomor-3)
    - Deskripsi Soal
    - [Kendala Nomor 3](#kendala-nomor-3)
    - [Solusi Nomor 3](#solusi-nomor-3)
    - [Source Code Nomor 3](https://gitlab.com/Anggito02/soal-shift-sisop-modul-2-b08-2022/-/tree/main/soal3)
    
## Anggota Kelompok

- Anggito Anju Hartawan Manalu	        5025201216
- Muhammad Yusuf Singgih Maulana	    5025201146
- Izzati Mukhammad	                    5025201075

## Nomor 1

### Deskripsi Soal

Pada soal ini kita diminta untuk (1a) melakukan download pada sebuah zip file yang berisikan data characters dan weapons dari sebuah item gatcha, kemudian mengekstrak file tersebut dan membuat sebuah directory gatcha_gatcha sebagai working directory. Kemudian (1b) mengambil data gatcha dengan ketentuan jika jumlah gatchanya genap maka akan akan dilakukan gatcha weapons dan jika ganjil maka akan dilakukan gatcha characters, setiap 10 data gatcha yang diambil akan dimasukan ke sebuah file dan setiap 90 kali gatcha akan dilakukan pembuatan folder baru untuk pengambilan data gatcha berikutnya. Untuk (1c) format penamaan dari filenya adalah  {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gatcha_270. (1d) Isi dari file yang telah dibuat adalah data dari hasil gatcha yang dilakukan dengan format isi datanya adalah {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}, contoh isi data sebagai berikut, 157_characters_5_Albedo_53880. Setiap kali gatcha memerlukan sebanyak 160 pimogems dan primogems yang di generate dia awal adalah sebanyak 79000 primogems. (1e) Gatcha gatcha dilakukan pada tanggal 30 Maret jam 04:44, kemudian setelah 3 jam hasil gatchanya akan di zip dengan nama not_safe_for_wibu dengan password "satuduatiga", kemudian melakukan pengahpusan folder sehingga hanya menyisakan file .zip saja

### Kendala Nomor 1

- Kendala pada nomor ini adalah saat ingin memasukan data hasil gatcha yang diambil dari database item gatcha, pada awalnya program tidak menampilkan data gatchanya secara menyeluruh sehingga menampilkan error segmentation fault.

- Kemudian kendala berikutnya adalah saat program ingin dijalankan dengan menggunakan daemon dan menunggu hingga jam 4:44 program malah mengeluarkan output yang salah

### Solusi Nomor 1

- Penyelesaian dari permasalah diatas dapat digunakan dengan menggunaan syntax `snprintf()` untuk menyimpan sebuah string yang nantinya kaan dimasukna ke dalam file dan `free()` yang digunakan untuk membebaskan memory yang digunakan saat menyimpan data rerity dan nama dari sebuah item gatcha.

- Untuk kendala kedua masih belum ditemukan solusi yang optimalnya

#### 1.a
Pada soal nomor ini kita diminta untuk melakukan downlaod database untuk gatcha yang ada pada link yang diberikan kemudian membuat sebuah folder gatcha_gatcha sebagai working directory

    void downloadChara(char path[]){
        if (fork() == 0){
            char *chara[] = {"wget", path, NULL};
            execv("/bin/wget", chara);
        } while ((wait(&status)) > 0);
    }

Dengan membuat sebuah fungsi untuk mendownload database untuk characters dengan menggabungkan fungsi `fork()`, `wait()` dan `execv()` dengan `wget` sebagai argumennya.

    void downloadWeapon(char path[]){
        if (fork() == 0){
            char *weapon[] = {"wget", path, NULL};
            execv("/bin/wget", weapon);
        } while ((wait(&status)) > 0);
    }

Sama halnya dengan fungsi yang digunakan untuk mendownload database dari characters, download databse dari weapons juga menggunakan fungsi `fork()`, `wait()` dan `execv()`.

kemudian mwlakukan download database dari characters dan weapon dengan memanggi fungsi diatas sebagai berikut

    downloadWeapon("https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download");
    sleep(1);
    downloadChara("https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download");
    sleep(1);

Karena hasil downloadannya belum berbentuk zip maka dilakukan pengubahan nama dengan detai kode sebagai berikut

    void chname(char cur[], char last[]){
    if (fork() == 0)
        execlp("mv", "mv", cur, last, NULL);
    while ((wait(&status)) > 0);
    }

Kemudian melakukan panggilan dari fungsi diatas dengan detail sebagai berikut

    chname("uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "weapons.zip");
    chname("uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "characters.zip");

Setelah itu dilakukanlah pengekstrakan file .zip yang sudah didownload dengan membuat sebuah fungsi untuk melakukan unzip

    void unZip(char file[]){
    if (fork() == 0)
        execlp("unzip", "-d", file, NULL);
    while ((wait(&status)) > 0);
    }

Dengan melakukan panggilan sebagai berikut

    unZip("weapons.zip");
    unZip("characters.zip");

Kemudian membuat sebuah folder gatcha gatcha dengan membuat fungsi untuk membuat folder terlebih dahulu 

    void mDir(char path[]){
        if (fork() == 0){
            execlp("mkdir", "-p", path, NULL);
        } while ((wait(&status)) > 0);
    }

Dengan memanggil fungsi tersebut sebagai berikut 

    mDir("gatcha_gatcha");


#### 1.b, c, d

Pada bagian ini dapat diselesaikan dengan membuat sebuah program yang digunakan untuk mengambil data yang ada pada folder characters dan weapons yang sudah diunzip sebelumnya dengan membuat program sebagai berikut

    char* pickChara(char path[]){
        DIR* dir = opendir(path);

        int pick, i=1;
        srand(time(0));
        pick = rand() % 48;

        struct dirent* entity;
    
        while(entity = readdir(dir)){
            if(i==pick){
                return entity->d_name;
            }++i;
        } closedir(dir);
    }

    char* pickWeap(char path[]){
        DIR* dir = opendir(path);

        int pick, i=1;
        srand(time(NULL));
        pick = rand() % 130;

        struct dirent* entity;
        
        while(entity = readdir(dir)){
            if(i==pick){
                return entity->d_name;
            }++i;
        } closedir(dir);
    }

- Perbedaan pada keua fungsi diatas hanya pada variabel `pick` nya saja jika pada pickChara nilai dari variabel `pick` nya adalah `pick = rand() % 48` hal ini karena pada database characters terdapat total data dari 48 characters saja sedangkan pada `pick = rand() % 130` hal ini karena pada databse weapons terdapat total data dari 130 weapons.

- pada kedua fungsi ini juga menggunkan syntax `srand()` yang digunakan untuk mengubah tiap data dari gathcha yang akan diambil sedangkan untuk `rand()` sendiri merupakan ramdomize generator pada bahasa C yang digunakna untuk mendapatkan nomor secara acak.

Kemudian karena databse yang ada memiliki ekstensi file berupa `.json` maka akan membuat sebuah fungsi untuk mengambil 2 properties yaitu berupa nama item gatcha dan raritynya dengan detail sebagai berikut

    char* gatcha(char path[], char* func){
        FILE *fp;
        char buffer[10240];
        char* data=(char*)calloc (10240, sizeof (char));
        char dataItem[70];

        struct json_object *parsed_json;
        struct json_object *name;
        struct json_object *rarity;

        sprintf(dataItem, "%s/%s", path, func);
        
        fp = fopen(dataItem, "r");
        fread(buffer, 10240, 1, fp);
        fclose(fp);

        parsed_json = json_tokener_parse(buffer);

        json_object_object_get_ex(parsed_json, "name", &name);
        json_object_object_get_ex(parsed_json, "rarity", &rarity);

        sprintf(data, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
        return data;
    }

Fungsi diatas dibuat dengan mengambil referensi dari https://progur.com/2018/12/how-to-parse-json-in-c.html, dengan menggunakan syntax `sprintf()` sebagai penyimpan string dari kedua properties yang akan diambil.

Kemudian membuat sebuah fungsi yang digunakan untuk membuat sebuah folder dan file yang didalamnya terdapat isi dari hasil gatcha gatcha yang telah dilakukan

    void forkygy(char argumen[], char path[]){
            if (fork() == 0){
                if(argumen == "mkdir") execlp(argumen, "-p", path, NULL);
                else execlp(argumen, argumen, path, NULL);
            } while ((wait(&status)) > 0);
    }

    void wangywangy(){
        int i=1, pity=0;
        char folder[30], file[30], hasil[100];
        FILE *fp;
        chdir("gatcha_gatcha");
        while(PRIMO > 159){
            PRIMO-=160;
            if((pity%90)==0){
                if(pity>=90) chdir("..");
                if(pity==450){
                    pity=403;
                    sprintf(folder, "total_gatcha_%d", pity+90);
                    forkygy("mkdir", folder);
                    pity=450;
                }    
                else {
                    sprintf(folder, "total_gatcha_%d", pity+90);
                    forkygy("mkdir", folder);
                }
                chdir(folder);
            } 
            if((pity%10)==0){
                char buf[BUF_LEN] = {0};
                time_t t; 
                struct tm *local, *gm;
                t = time(NULL);
                local = localtime(&t);
                memset(buf, 0, BUF_LEN);
                strftime(buf, BUF_LEN, "%T", local);
                if(pity==490){
                    pity=483;
                    sprintf(file, "%s_gatcha_%d.txt", buf, pity+10);            
                    sleep(1);
                    forkygy("touch", file);
                    pity=490;
                } else{
                    sprintf(file, "%s_gatcha_%d.txt", buf, pity+10);
                    sleep(1);
                    forkygy("touch", file);
                }
            } pity++; 
            if(pity&1==1){
                fp = fopen(file, "a+");
                snprintf(hasil, 100, "%d_characters_%s_%d\n", pity, gatcha(pathChara, pickChara(pathChara)), PRIMO);
                fprintf(fp, "%s", hasil);
                fclose(fp);
                free(gatcha(pathChara, pickChara(pathChara)));
            } else{
                fp = fopen(file, "a+");
                snprintf(hasil, 100, "%d_weapons_%s_%d\n", pity, gatcha(pathWeap, pickChara(pathWeap)), PRIMO);
                fprintf(fp, "%s", hasil);
                fclose(fp);
                free(gatcha(pathWeap, pickChara(pathWeap)));
            } 
        } chdir("..");
        chdir("..");
    }

- Pada kode diatas pertama kita melakukan perulangan dengan kondisi jika primogems yang ada lebih dari 159 maka akan dilakukan gatcha gatcha, kemudian membuat sebuah folder setiap 90 kali gatcha dengan format yang sudah ditentukan dengan membuat fungsi `forkygy()`.
- Fungsi diatas sebenarnya sama saja dengan kegunaan dari fungsi mDir yang sudah di sebutkan sebelumnya.
- Setelah itu membuat sebuah file setiap 10 kali gatcha yang nantinya file tersebut memiliki isi dari hasil gatcha yang telah dilakukan dengan menggunakan fungsi localtime sebagai waktu yang nantinya akan disimpan pada format file dan setiap pembuatan file memiliki jeda 1 detik.
- Kemudian membuka file dan mengisinya dengan data yang sudah diambil pada fungsi `gatcha()`, jika gatchanya ganjil maka akan mengambil data dari characters dan jika genap akan mengambil dari weapons.


#### 1.e

Pada nomor ini program akan melakukan zip 3 jam setelah jam 4:44 pada tanggal 30 Maret dan memberinya password `satuduatiga` dan melakukan penghapusan sehingga hanya terdapat file .zip saja dengan menggunan fungsi sebagai berikut

    void nswf(){
        pid_t child_id;
        int status;

        child_id = fork();

        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }
        if (child_id == 0) {
            // this is child
            char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "gatcha_gatcha", NULL};
            execv("/bin/zip", argv);
        } else {
            // this is parent
            while ((wait(&status)) > 0);
            char *argv[] = {"rm", "-r", "gatcha_gatcha", "characters", "weapons", "characters.zip", "weapons.zip", NULL};
            execv("/bin/rm", argv);
        }
    }

Untuk melakukan proses dan melakukan zip pada folder gatcha gatcha, memerlukan kode sebagai berikut 

    time_t gatchaygy, nsfw;
    gatchaygy = 1648590240;
    nsfw = 1648601040;
    
Kode diatas dimaksudkan untuk memulai proses pada jam 4:44 dan zip pada jam 7:44

Berikut merupakan bukti screenshot dari program yang telah dijalankan
![isizip!](img/1/pw.png)
![isigatcha!](img/1/hasil_gatcha.png)
![isifolder!](img/1/isi_folder.png)

# Nomor 2

### Deskripsi Soal
Pada soal nomor 2, diberikan folder berbentuk zip yang berisi poster-poster drama korea dengan nama <b>drama.zip</b>. Folder zip ini kemudian akan di-proses untuk tujuan tertentu (sesuai sub-soal) dengan syarat-syarat tertentu yang diberikan oleh sub-soal. 

(2.a) Folder zip harus di-extract ke dalam folder `/home/{user}/shift2/drakor`. Selain poster-poster drama korea, folder zip ini juga memiliki folder-folder tidak penting lain yang selanjutnya tidak akan digunakan sehingga harus dihapus. Folder `shift2` dan `drakor` juga harus dibuat otomatis.

(2.b-d) Poster-poster yang berada di folder zip ini harus dipindahkan/dicopy sesuai dengan kategori yang tertera pada nama file (Format nama file: `{judul-film};{tahun-rilis};{kategori-film}`). Misalnya poster dengan nama file `start-up;2020;romance_the-k2;2021;action.png` harus di copy pada folder `~/shift2/drakor/romance` dan `~/shift2/drakor/action` 

(2.c) dengan nama file `{judul-film}.png`.

(2.e) Setiap direktori kategori harus memiliki file `data.txt`. `data.txt` akan berisi list informasi semua film dari setiap kategorinya. `data.txt` memiliki format

    kategori : {kategori-film}

    nama : {judul-film}
    rilis : {tahun-rilis}

    .
    .
    dst.

Selain itu, informasi yang dilist di dalam `data.txt` harus bersifat <i>Ascending</i> menurut `{tahun-rilis}`.

### Kendala Nomor 2

- <i>Directory Listing</i> yang harus dilakukan untuk menyelesaikan permasalahan nomor 2 cukup banyak sehingga memakan waktu yang cukup besar hanya untuk melakukan <i>loop listing</i>

- `strtok()` dari library `string.h` memiliki kelemahan jika dipakai dengan sifat <i>nested</i>. Misalnya

<pre><code>char string[10] = "He;ll;o_S;is;op";
char *substring = strtok(string, "_");
char *sub_substring = strtok(substring, ";");

while(sub_substring != NULL) {
    printf("%s\n", sub_substring);
    sub_substring = (NULL, ";");
}</code></pre>

akan terjadi <i>Segmentation Fault</i> meskipun logic yang digunakan seharusnya sudah benar.

### Solusi Nomor 2
- Tidak ada solusi untuk <i>directory listing</i> yang banyak dari solusi nomor 2, kecuali mengganti <i>logic</i> penyelesaian masalah untuk nomor 2.

- Untuk kekurangan pada `strtok()` dapat diatasi dengan pemakaian `strtok()` tanpa harus nesting. Akan tetapi, hal ini menyebabkan `strtok()` tidak efektif.

#### Penjelasan singkat pada sub-processes
- Akan dibuat 7 child process yang terhubung langsung dengan parent untuk meng-<i>handle</i> proses-proses utama.

- Berikut adalah proses utama tiap child process process yang terhubung langsung dengan parent.

- `child_a` = membuat direktori `shift2`
- `child_b` = membuat direktori `drakor`
- `child_c` = `unzip/extract` drakor.zip
- `child_d` = membuat direktori untuk masing-masing kategori dan menghapus folder tidak penting
- `child_e` = `copy` masing-masing poster ke direktori tujuan (merupakan anak langsung dari `child_d` agar mendapat informasi `struct posterInfo`)
- `child_f` = membuat `data.txt`
- `child_g` = listing informasi untuk masing-masing `data.txt` (merupakan anak langsung dari `child_d` agar mendapat informasi `struct posterInfo`)

#### 2a. Membuat direktori shift2, drakor, extract drakor.zip dan menghapus folder tidak penting

Kita dapat membuat direktori baru dengan menggunakan `mkdir`. Selain itu, kita juga memakai `-p` untuk meng-<i>handle</i> error yang muncul saat direktori sudah ada.

Kita juga harus menggunakan fungsi `exec()` dari library `unistd.h`. Kita juga harus membuat child-process dari parent-process karena fungsi `exec()` akan otomatis melakukan <i>terminate</i> dari proses yang menjalankan.
<br>
<br>
<img src="/img/2/gambar-2-a-1.png" alt="mkdir_shift2">
<br>
<br>
<img src="/img/2/gambar-2-a-2.png" alt="mkdir_drakor">

Kita juga harus menggunakan fungsi `wait()` atau `waitpid()` untuk menunggu proses lain diselesaikan. Misalnya untuk membuat direktori `drakor`, tentu direktori `shift2` harus selesai terlebih dahulu. Pada kasus ini, `child_a` adalah proses untuk membuat direktori `shift2` dan `child_b` adalah proses untuk membuat direktori `drakor`.

Untuk menghapus folder tidak penting kita dapat mudah menggunakan fungsi `strstr()` dan mengecek apakah string mengandung substring "." karena file yang mengandung "." bukan merupakan folder. Setelah itu, barulah kita menggunakan `rm -r` untuk menghapus direktori yang tidak diperlukan. Masalah ini diselesaikan oleh sub-proses `child-d`
<br>
<br>
<img src="/img/2/gambar-2-a-3.png" alt="remove unrelated">

Untuk meng-<i>extract</i> drakor.zip, kita akan menggunakan `exec()` dalam sub-process `child_c`.

#### 2b. Membuat direktori untuk setiap kategori

Kita tidak memiliki informasi mengenai tiap jenis kategori. Maka dari itu, kita perlu melakukan <i>directory listing</i> untuk setiap poster untuk mendapatkan kategori apa saja yang ada dalam folder yang diberikan.
<br>
<br>
<img src="/img/2/gambar-2-b-1.png" alt="dir_list_for_cats">

Setelah mendapatkan semua kategori yang dibutuhkan, kita dapat membuat direktori untuk setiap kategori. Kita juga menggunakan `-p` karena merupakan hal yang mungkin direktori dengan nama yang sama dibuat.
<br>
<br>
<img src="/img/2/gambar-2-b-2.png" alt="mkdir_cats">

Agar menambah efisiensi, kita dapat mengumpulkan informasi terkait poster pada sub-proses ini, yakni `child_d` dan menyimpannya pada `struct posterInfo[]` agar memudahkan penyelesaian untuk sub-proses selanjutnya. Kita menggunakan fungsi `strtok()` untuk memisahkan informasi dari setiap file poster.

#### 2c-d. Memindahkan poster ke folder sesuai dan <i>rename</i> file

Untuk memindahkan setiap poster, lagi-lagi kita menggunakan <i>directory listing</i> pada folder drakor. 
Untuk menentukan direktori mana yang cocok untuk poster tertentu, kita dapat menggunakan fungsi `strstr()` untuk menentukan apakah sebuah nama file poster mengandung informasi {posterInfo[].kategori-film} yang telah didapatkan dari sub-proses `child_d`. Jika iya, maka <i>copy</i> poster tersebut pada direktori terkait. Oleh karena kita sudah memisahkan informasi film yang dipisahkan oleh `_` menggunakan `strtok()` pada `child_d`
<br>
<br>
<img src="/img/2/gambar-2-cd-1.png" alt="strtok_png">

Dan kita telah menggunakan fungsi `strstr()` untuk menentukan poster sesuai kategori, masalah harus meng-<i>copy</i> 1 poster ke 2 direktori yang memungkinkan sudah dapat diatasi.

#### 2e. Membuat listing film/kategori di data.txt

Untuk menyelesaikan masalah ini kita harus:
1. Membuat `data.txt` pada setiap direktori kategori
2. Membuat priority queue agar membuat listing bersifat <i>ascending</i> menurut tahun rilis.
3. <i>Directory listing</i> pada setiap direktori kategori
4. Membuat listing sesuai format soal
5. Menuliskannya pada tiap `data.txt`

<b>Langkah (1)</b>
Untuk membuat data.txt kita haru melakukan `chdir()` ke setiap direktori kategori. Setelah melakukan `chdir()`, barulah kita dapat menggunakan fungsi `exec()` untuk membuat `data.txt`. Langkah (1) dapat diselesaikan oleh `child_f`.
<br>
<br>
<img src="/img/2/gambar-2-e-1.png" alt="touch_data.txt_child_f">

<b>Langkah (2)</b>
Dalam membuat sistem/tipe data priority_queue, kita dapat membuatnya sebagai berikut
<br>
<br>
<img src="/img/2/gambar-2-e-2.png" alt="pq_and_pqnode struct">
<br>
<br>
<img src="/img/2/gambar-2-e-3.png" alt="pq_init_isEmpty">
<br>
<br>
<img src="/img/2/gambar-2-e-4.png" alt="pq_push_pop">

<b>Langkah (3)</b>
Kita harus melakukan listing untuk setiap film dalam sebuah direktori kategori. Setelah itu, kita menyimpan data {tahun-rilis} dan {judul-film} pada tiap node sehingga dapat dibuat <i>ascending</i> sesuai logic priority queue.
<br>
<br>
<img src="/img/2/gambar-2-e-5.png" alt="pq_push_data">

<b>Langkah (4)</b>
Kita dapat menggunakan fungsi `strcat()` untuk mengatur listing data sesuai format. Selain itu, kita juga menggunakan fungsi `sprintf()` untuk mengubah int->string.
<br>
<br>
<img src="/img/2/gambar-2-e-6.png" alt="formatting_datalist">

<b>Langkah (5)</b>
Untuk menuliskan listing pada file data.txt, kita harus menggunakan fungsi `fopen()` untuk membuka file yang akan diedit.
<br>
<br>
<img src="/img/2/gambar-2-e-7.png" alt="open_file">

Kemudian kita menggunakan fungsi `fprintf()` untuk menuliskan listing data.
<br>
<br>
<img src="/img/2/gambar-2-e-8.png" alt="fprintf">

Tidak lupa kita berikan fungsi `fclose()` dan `closedir()` untuk menutup semua stream.

# Nomor 3

### Deskripsi Soal
Pada soal nomor 3, kita akan diberikan sebuah folder zip bernama animal.zip yang berisi file-file gambar binatang yang hilang. File-file ini akan digunakan untuk menemukan binatang apa saja yang hilang. Kemudian file-file ini diproses untuk diambil informasinya sesuai sub-soal yang diberikan.

(3.a) Kita akan membuat direktori dengan nama `darat` dan `air` untuk memisahkan binatang tinggal di darat dan air. Akan tetapi, direktori `air` harus dibuat 3 detik setelah direktori `darat` sudah dibuat.

(3.b) Kemudian kita diminta untuk meng-<i>extract</i> data dari `animal.zip` ke `/home/[USER]/modul2/`

(3.c) Hasil data <i>extract</i> berisi file-file gambar binatang yang harus dipisahkan berdasarkan tempat hidupnya. Beberapa binatang memiliki keterangan tempat hidup dan beberapa tidak. File binatang yang memiliki keterangan tempat hidup harus dipindahkan pada direktori sesuai tempat hidupnya. Misalnya file dengan nama `ant_darat.png` harus dipindahkan pada direktori `darat`. Sedangkan untuk hewan yang tidak memiliki keterangan tempat hidup harus dihapus dari direktori `/animal`.

(3.d) Karena terlalu banyak binatang yang hilang, binatang dengan informasi `bird` harus dihapus dari direktori `/darat`.

(3.e) Terakhir, akan dibuatkan file list.txt pada direktori `/air` untuk listing binatang air yang hilang. Listing ini memiliki format
    UID_[UID file permission]_Nama File.[jpg/png].

### Kendala Nomor 3
Terjadi bug pada saat penggunaan fungsi `sleep(n)`. Fungsi `sleep(n)` seharusnya membuat program menunggu selama n detik sebelum program dilanjutkan. Akan tetapi, saat dilakukan pengecekan menggunakan fungsi bash `stat`, ditemukan bahwa direktori dibuat secara bersamaan tanpa adanya jeda sebesar n=3 detik. Akan tetapi, jika dilihat langsung menggunakan UI OS, direktori air tepat dibuat setelah 3 detik direktori darat dibuat.

### Solusi Nomor 3
Masih belum ditemukan solusi yang tepat untuk memperbaiki bug dalam fungsi `sleep()` ini.

#### Penjelasan singkat pada sub-processes
- Akan dibuat 8 child process yang terhubung langsung dengan parent untuk meng-<i>handle</i> proses-proses utama.

- Berikut adalah proses utama tiap child process process yang terhubung langsung dengan parent.

- `child_a` = membuat direktori `darat`
- `child_b` = membuat direktori `air`
- `child_c` = `unzip/extract` animal.zip
- `child_d` = memindahkan hewan pada direktori sesuai tempat hidup
- `child_e` = menghapus sisa binatang tanpa informasi tempat hidup
- `child_f` = menghapus `bird` pada direktori `/darat`
- `child_g` = membuat `list.txt` pada direktori `/air`
- `child_h` = menuliskan listing sesuai format yang diberikan oleh sub-soal pada `list.txt`

#### 3a. Membuat direktori darat dan air
Untuk membuat direktori baru dalam program C. Kita dapat menggunakan fungsi `exec()` dan menggunakan fungsi bash `mkdir`. Untuk memberikan jeda 3 detik dalam program, kita dapat menggunakan fungsi `sleep(3)`. Berikut kode yang dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-a-1.png" alt="mkdir_air_darat">

#### 3b. Extract animal.zip
Untuk meng-<i>extract</i> sebuah folder zip, kita juga memerlukan fungsi `exec()` dan fungsi bash `unzip -q`.`-q` disini digunakan agar proses unzip tidak membuat terminal harus menge-print proses unzip. Berikut kode yang dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-b-1.png" alt="unzip_animal.zip">

#### 3c. Memindahkan binatang sesuai tempat hidup
Untuk memindahkan file binatang sesuai tempat hidupnya, kita perlu melakukan <i>directory listing</i>. Hal ini dilakukan untuk mendapatkan informasi tempat hidup tiap binatang yang di-list. Kita menggunakan fungsi `strstr()` untuk mengecek apakah sebuah substring, yakni darat/air, berada dalam string nama file. Setelah itu, kita menggunakan fungsi `exec()` dan fungsi bash `mv` untuk memindahkan binatang pada direktori yang cocok. Berikut kode yang dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-c-1.png" alt="mv_air_darat">

#### 3d. Menghapus bird
Kita akan menggunakan <i>directory listing</i> lagi untuk mendapatkan informasi bird pada direktori `/darat`. Kita menggunakan `strstr()` untuk mengecek bird pada nama file. Jika ditemukan, kita akan menggunakan fungsi `exec()` dan `rm` untuk menghapus file yang mengandung informasi `bird` didalamnya. Berikut kode yang dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-d-1.png" alt="rm_bird">

#### 3e. Menuliskan data binatang pada /air
Dalam menuliskan data ke dalam sebuah file .txt, kita harus menggunakan tipe data <b>FILE</b> yang berisi data stream untuk file. Kita akan menggunakan fungsi `fopen()` untuk membuka file `list.txt` dalam format `w+` dimana kita dapat meng-<i>append</i> data yang ingin dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-e-1.png" alt="fopen">

Selanjutnya, kita akan melakukan pengambilan informasi UID dari file-file di dalam direktori /air. Hal ini dapat kita lakukan dengan menggunakan `struct stat` yang tersedia pada library `sys/stat.h`. Kita dapat menggunakan fungsi `stat()` untuk mendapatkan informasi UID dari file yang di pass. Jika file dapat di-read, maka akan me-<i>return</i> S_IRUSR. Jika dapat di-write, maka akan  me-<i>return</i> S_IWUSR. Jika dapat di-execute, maka akan  me-<i>return</i> S_IXUSR. Berikut kode yang dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-e-2.png" alt="UID">

Terakhir, kita dapat menggunakan fungsi `fprintf()` untuk menuliskan teks pada file .txt yang telah dibuka sebelumnya, yakni `list.txt`. Berikut kode yang dituliskan.
<br>
<br>
<img src="/img/3/gambar-3-e-3.png" alt="fprintf">
