#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>

int main() {
    // Childs pids with same main processes
    pid_t 
        child_a,
        child_b,
        child_c,
        child_d,
        child_e,
        child_f,
        child_g,
        child_h;

    // working directory
    char cwd[250];

    // change to /modul2/ working dir
    if(chdir("/home/anggito/modul2") < 0) perror("Error chdir to modul2!\n");

    // child_a ==> make darat dir
    child_a = fork();
    if(child_a == 0) {
        execl("/bin/mkdir", "mkdir_darat", "-p", "darat", NULL);
    }

    // wait a process done
    waitpid(child_a, NULL, 0);

    // child_b ==> make air dir
    child_b = fork();
    if(child_b == 0) {
        sleep(3);
        execl("/bin/mkdir", "mkdir_air", "-p", "air", NULL);
    }

    // child_c ==> unzip/extract animal.zip
    child_c = fork();
    if(child_c == 0) {
        execl("/bin/unzip", "unzip_animal", "-q", "/home/anggito/modul2/animal.zip", NULL);
    }

    // wait all processes to get done    
    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    waitpid(child_c, NULL, 0);
    
    // child_d ==> move animals
    child_d = fork();
    if(child_d == 0) {
        // change dir to animal dir
        if(chdir("/home/anggito/modul2/animal") < 0) perror("Error chdir to animal!\n");
        
        // dir listing for animal
        DIR *dir_path;
        struct dirent *curr_file;

        // darat listing
        dir_path = opendir("/home/anggito/modul2/animal");

        if(dir_path != NULL) {
            while(curr_file = readdir(dir_path)) {
                // find darat animals
                if(strstr(curr_file->d_name, "darat")) {
                    if(fork() == 0) {                    
                        execl("/bin/mv", "mv_darat", curr_file->d_name, "/home/anggito/modul2/darat", NULL);
                    }
                }
            }
        }
        closedir(dir_path);

        // air listing
        dir_path = opendir("/home/anggito/modul2/animal");

        if(dir_path != NULL) {
            while(curr_file = readdir(dir_path)) {
                // find air animals
                if(strstr(curr_file->d_name, "air")) {
                    if(fork() == 0) {
                        execl("/bin/mv", "mv_air", curr_file->d_name, "/home/anggito/modul2/air", NULL);
                    }
                }
            }
        }
        closedir(dir_path);
    }

    // wait till all animals moved
    while(waitpid(child_d, NULL, 0) > 0);

    // child_e ==> remove remaining animal
    child_e = fork();
    if(child_e == 0 && child_d > 0) {
        // change to /modul2/ working dir
        if(chdir("/home/anggito/modul2") < 0) perror("Error chdir to modul2!\n");
        execl("/bin/rm", "rm_animal", "-r", "animal", NULL);
    }

    while(waitpid(child_a, NULL, 0) > 0);
    while(waitpid(child_d, NULL, 0) > 0);

    // child f ==> remove bird
    child_f = fork();
    if(child_f == 0 && child_e > 0 & child_d > 0) {
        // printf("Hello\n");
        // change dir to darat animal dir
        if(chdir("/home/anggito/modul2/darat") < 0) perror("Error chdir to animal!\n");
        
        // dir listing for bird
        DIR *dir_path;
        struct dirent *curr_file;

        // darat listing
        dir_path = opendir("/home/anggito/modul2/darat");

        if(dir_path != NULL) {
            while(curr_file = readdir(dir_path)) {
                // find bird
                if(strstr(curr_file->d_name, "bird")) {
                    if(fork() == 0) {
                        execl("/bin/rm", "rm_bird", curr_file->d_name, NULL);
                    }
                }
            }
        }
        closedir(dir_path);
    }

    waitpid(child_a, NULL, 0);
    // child g ==> make list.txt
    child_g = fork();
    if(child_g == 0 && child_f > 0 && child_e > 0 && child_d > 0) {
        // printf("Hello\n");
        if(chdir("/home/anggito/modul2/air") < 0) perror("Error chdir to air make list.txt!\n");
        execl("/bin/touch", "touch_list.txt", "list.txt", NULL);
    }

    waitpid(child_b, NULL, 0);
    waitpid(child_d, NULL, 0);
    // child h ==> get file uid
    child_h = fork();
    if(child_h == 0 && child_g == 0 && child_f == 0 && child_e == 0) {
        if(chdir("/home/anggito/modul2/air") < 0) perror("Error chdir to air uid!\n");

        // open list.txt
        FILE *listTxt;
        listTxt = fopen("/home/anggito/modul2/air/list.txt", "w+");

        // DIR listing for air animals
        DIR *dir_path;
        struct dirent *curr_file;

        // darat listing
        dir_path = opendir("/home/anggito/modul2/air");

        if(dir_path != NULL) {
            while(curr_file = readdir(dir_path)) {
                // find bird
                if(strstr(curr_file->d_name, "air")) {
                    // info container
                    char dir_air_path[256] = "/home/anggito/modul2/air/";
                    char fileInfoContainer[256] = "";

                    // get file info variables
                    struct stat info;

                    // get infos
                    strcat(dir_air_path, curr_file->d_name);
                    stat(dir_air_path, &info);
                    struct passwd *pw = getpwuid(info.st_uid);

                    // uid
                    if(pw != 0) strcat(fileInfoContainer, pw->pw_name);
                    strcat(fileInfoContainer, "_");

                    // uid file info
                    if(info.st_mode & S_IRUSR) strcat(fileInfoContainer, "r");
                    if(info.st_mode & S_IWUSR) strcat(fileInfoContainer, "w");
                    if(info.st_mode & S_IXUSR) strcat(fileInfoContainer, "x");
                    strcat(fileInfoContainer, "_");

                    // file name
                    strcat(fileInfoContainer, curr_file->d_name);

                    // append to list.txt
                    fprintf(listTxt, "%s\n", fileInfoContainer);
                }
            }
        }
        fclose(listTxt);
        closedir(dir_path);
    }
}