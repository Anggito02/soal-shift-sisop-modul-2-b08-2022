#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <json-c/json.h>
#include <stdio.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#define BUF_LEN 256


int status, PRIMO = 79000;
char pathChara[] = "/home/nauli/Documents/praktikum_sisop/soal/modul2/characters";
char pathWeap[] = "/home/nauli/Documents/praktikum_sisop/soal/modul2/weapons";

void checkpid(pid_t pid){
    if(getpid() != pid) exit(EXIT_SUCCESS);
}

void chname(char cur[], char last[]){
    if (fork() == 0)
        execlp("mv", "mv", cur, last, NULL);
    while ((wait(&status)) > 0);
}

void unZip(char file[]){
    if (fork() == 0)
        execlp("unzip", "-d", file, NULL);
    while ((wait(&status)) > 0);
}

void Zip(char file[]){
    if (fork() == 0)
        execlp("zip", "-d", file, NULL);
    while ((wait(&status)) > 0);
}

void downloadChara(char path[]){
    if (fork() == 0){
      char *chara[] = {"wget", path, NULL};
      execv("/bin/wget", chara);
    } while ((wait(&status)) > 0);
}

void downloadWeapon(char path[]){
    if (fork() == 0){
      char *weapon[] = {"wget", path, NULL};
      execv("/bin/wget", weapon);
    } while ((wait(&status)) > 0);
}

void mDir(char path[]){
    if (fork() == 0){
        //char *argv[] = {"mkdir", "-p", path, NULL};
        execlp("mkdir", "-p", path, NULL);
    } while ((wait(&status)) > 0);
}

void forkygy(char argumen[], char path[]){
    if (fork() == 0){
        if(argumen == "mkdir") execlp(argumen, "-p", path, NULL);
        else execlp(argumen, argumen, path, NULL);
    } while ((wait(&status)) > 0);
}

void mFile(char path[]){
    if (fork() == 0){
        char *argv[] = {"touch", path, NULL};
        execv("/usr/bin/touch", argv);
    } while ((wait(&status)) > 0);
}

char* pickChara(char path[]){
    DIR* dir = opendir(path);
    //if(dir == NULL) return 1;

    int pick, i=1;
    srand(time(0));
    pick = rand() % 48;


    struct dirent* entity;
    
    while(entity = readdir(dir)){
        if(i==pick){
            return entity->d_name;
            // printf("%s\n", entity->d_name);
            // printf("%d\n", pick);
        }++i;
    } closedir(dir);
}

char* pickWeap(char path[]){
    DIR* dir = opendir(path);
    //if(dir == NULL) return 1;

    int pick, i=1;
    srand(time(NULL));
    pick = rand() % 130;


    struct dirent* entity;
    
    while(entity = readdir(dir)){
        if(i==pick){
            return entity->d_name;
            // printf("%s\n", entity->d_name);
            // printf("%d\n", pick);
        }++i;
    } closedir(dir);
}

char* gatcha(char path[], char* func){
    FILE *fp;
    char buffer[10240];
    char* data=(char*)calloc (10240, sizeof (char));
    char dataItem[70];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    sprintf(dataItem, "%s/%s", path, func);
    
    fp = fopen(dataItem, "r");
    fread(buffer, 10240, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "name", &name);
	json_object_object_get_ex(parsed_json, "rarity", &rarity);

    sprintf(data, "%d_%s", json_object_get_int(rarity), json_object_get_string(name));
    // sprintf(dataRarity, "%d", json_object_get_int(rarity));
    // sprintf(dataItem, "%s", json_object_get_int(name));
    return data;
}

void wangywangy(){
    int i=1, pity=0;
    char folder[30], file[30], hasil[100];
    FILE *fp;
    chdir("gatcha_gatcha");
    while(PRIMO > 159){
        PRIMO-=160;
        //buat folder total gatcha
        //mDir("gatcha_gatcha/total_gatcha_90");
        //if folder gatcha %90 == 0 buat folder baru 
        if((pity%90)==0){
            if(pity>=90) chdir("..");
            if(pity==450){
                pity=403;
                sprintf(folder, "total_gatcha_%d", pity+90);
                forkygy("mkdir", folder);
                pity=450;
            }    
            else {
                sprintf(folder, "total_gatcha_%d", pity+90);
                forkygy("mkdir", folder);
            }
            chdir(folder);
        } 
        // buat file gatcha di folder gatcha
        // if data file gatcha %10 == 0 buat file baru
        if((pity%10)==0){
            char buf[BUF_LEN] = {0};
            time_t t; 
            struct tm *local, *gm;
            t = time(NULL);
            local = localtime(&t);
            memset(buf, 0, BUF_LEN);
            strftime(buf, BUF_LEN, "%T", local);
            if(pity==490){
                pity=483;
                sprintf(file, "%s_gatcha_%d.txt", buf, pity+10);            
                sleep(1);
                forkygy("touch", file);
                pity=490;
            } else{
                sprintf(file, "%s_gatcha_%d.txt", buf, pity+10);
                sleep(1);
                forkygy("touch", file);
            }

        } pity++; 
        // masukin data gatcha
        // update file gatcha
        //sleep(1);
        if(pity&1==1){
            fp = fopen(file, "a+");
            snprintf(hasil, 100, "%d_characters_%s_%d\n", pity, gatcha(pathChara, pickChara(pathChara)), PRIMO);
            fprintf(fp, "%s", hasil);
            fclose(fp);
            free(gatcha(pathChara, pickChara(pathChara)));
        } else{
            fp = fopen(file, "a+");
            snprintf(hasil, 100, "%d_weapons_%s_%d\n", pity, gatcha(pathWeap, pickWeap(pathWeap)), PRIMO);
            fprintf(fp, "%s", hasil);
            fclose(fp);
            free(gatcha(pathWeap, pickChara(pathWeap)));
        } //sleep(1);
    } chdir("..");
    chdir("..");
}

void nswf(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    if (child_id == 0) {
        // this is child
        char *argv[] = {"zip", "--password", "satuduatiga", "-r", "not_safe_for_wibu.zip", "gatcha_gatcha", NULL};
        execv("/bin/zip", argv);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        char *argv[] = {"rm", "-r", "gatcha_gatcha", "characters", "weapons", "characters.zip", "weapons.zip", NULL};
        execv("/bin/rm", argv);
    }
}

int main() {
//   pid_t pid;        // Variabel untuk menyimpan PID

//   pid = fork();     // Menyimpan PID dari Child Process

//   if (pid < 0) exit(EXIT_FAILURE);
//   if (pid > 0) exit(EXIT_SUCCESS);
//   if (setsid() < 0) _Exit(EXIT_FAILURE);
//   pid = fork();
//   if (pid < 0) exit(EXIT_FAILURE);
//   if (pid > 0) exit(EXIT_SUCCESS);
//   umask(0);
  
//   close(STDIN_FILENO);
//   close(STDOUT_FILENO);
//   close(STDERR_FILENO);

//     while (1) {
    // Tulis program kalian di sini
        time_t gatchaygy, nsfw;
        gatchaygy = 1648590240;
        nsfw = 1648601040;

        downloadWeapon("https://drive.google.com/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download");
        sleep(1);
        downloadChara("https://drive.google.com/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download");
        sleep(1);
        chname("uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "weapons.zip");
        chname("uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "characters.zip");
        unZip("weapons.zip");
        unZip("characters.zip");
        mDir("gatcha_gatcha");
        //while(time(NULL) < gatchaygy); 
        wangywangy();
        //while(time(NULL) < nsfw); 
        nswf();
        //sleep(30);
    // }
    return 0;
}