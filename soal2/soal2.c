#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>

typedef struct POSTER_INFO
{
    char title[80];
    char category[80];
    int year;
} POSTER_INFO;


// PQueues for ascending listing
typedef struct pqueueNode {
    char title[80];
    int year;
    struct pqueueNode *next;
} PQNode;

typedef struct pqueue {
    PQNode *top;
    unsigned size;
} PQ;

// init pqueue
void pqueue_init(PQ *pqueue) {
    pqueue->top = NULL;
    pqueue->size = 0;
}

// check if pqueue isEmpty
bool pqueue_isEmpty(PQ *pqueue) {
    return (pqueue->top == NULL);
}

// push pqueue
void pqueue_push(PQ *pqueue, char *title, int year) {
    PQNode *temp = pqueue->top;
    PQNode *newNode = (PQNode*) malloc(sizeof(PQNode));
    strcpy(newNode->title, title);
    newNode->year = year;
    newNode->next = NULL;

    if(pqueue_isEmpty(pqueue)) {
        pqueue->top = newNode;
        pqueue->size++;
        return;
    }

    if(year < pqueue->top->year) {
        newNode->next = pqueue->top;
        pqueue->top = newNode;
    }
    else {
        while(temp->next != NULL && temp->next->year <= year) {
            temp = temp->next;
        }

        newNode->next = temp->next;
        temp->next = newNode;
    }
    pqueue->size++;
}

// pop top of pqueue
void pqueue_pop(PQ *pqueue) {
    if(!pqueue_isEmpty(pqueue)) {
        PQNode *temp = pqueue->top;
        pqueue->top = pqueue->top->next;
        free(temp);
        pqueue->size--;
    }
}

int main() {
    POSTER_INFO posterInfo[100];

    // First gen child's pid
    pid_t
        child_a,
        child_b,
        child_c,
        child_d,
        child_e,
        child_f,
        child_g;

    // film indexing
    int filmIndex = 0;

    // status handler
    int status;

    child_a = fork();
    // make shift2 dir
    if(child_a == 0)  {
        execl("/bin/mkdir", "mkdir_shift2", "-p","/home/anggito/shift2", NULL);
    }

    waitpid(child_a, NULL, 0);
    child_b = fork();
    // make drakor dir
    if(child_b == 0) {
        execl("/bin/mkdir", "mkdir_drakor", "-p", "/home/anggito/shift2/drakor", NULL);
    }

    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    child_c = fork();
    // unzip drakor.zip
    if(child_c == 0 && child_a > 0 && child_b > 0) {
        // change working dir to Downloads
        if(chdir("/home/anggito/Downloads") < 0) perror("Error chdir to Downloads!\n");

        execl("/bin/unzip", "unzip_drakor.zip", "-q", "drakor.zip", "-d", "/home/anggito/shift2/drakor", NULL);
    }

    waitpid(child_a, NULL, 0);
    waitpid(child_b, NULL, 0);
    waitpid(child_c, NULL, 0);
    child_d = fork();
    // make dir for each category
    // remove unrelated folders
    if(child_d == 0 && child_c > 0 && child_b > 0 && child_a > 0) {
        if(chdir("/home/anggito/shift2/drakor") < 0) perror("Error chdir to drakor!\n");

        DIR *dir_path;
        struct dirent *curr_file;

        // opendir cwd
        dir_path = opendir("/home/anggito/shift2/drakor");

        // dir listing drakor
        if(dir_path != NULL) {
            char tempFilmSeparator[30][50];
            while(curr_file = readdir(dir_path)) {
                if(strcmp(curr_file->d_name, ".") != 0 && strcmp(curr_file->d_name, "..") != 0) {
                    // process remaining files
                    if(strstr(curr_file->d_name, ".png")) {
                        // png separator
                        char *pngSeparator = strtok(curr_file->d_name, ".");

                        // extract fiLm per poster
                        char *filmSeparator = strtok(pngSeparator, "_");

                        while(filmSeparator != NULL) {
                            strcpy(tempFilmSeparator[filmIndex], filmSeparator);

                            filmIndex++;
                            filmSeparator = strtok(NULL, "_");
                        }
                    }
                    else {
                        // remove unrelated folders
                        char unrelatedFoldersPath[256] = "/home/anggito/shift2/drakor/";
                        strcat(unrelatedFoldersPath, curr_file->d_name);
                        if(fork() == 0) execl("/bin/rm", "rm_unrelated", "-r", unrelatedFoldersPath, NULL);
                    }
                }
            }

            // get info per film
            for(int i = 0; i<filmIndex; i++) {
                char *infoSeparator = strtok(tempFilmSeparator[i], ";");
                strcpy(posterInfo[i].title, infoSeparator);

                infoSeparator = strtok(NULL, ";");
                posterInfo[i].year = atoi(infoSeparator);

                infoSeparator = strtok(NULL, ";");
                strcpy(posterInfo[i].category, infoSeparator);                
            }

            // make dir for each category
            for(int i = 0; i<filmIndex; i++) {
                if(fork() == 0) execl("/bin/mkdir", "mkdir_cat", "-p", posterInfo[i].category, NULL);
            }
        }
        closedir(dir_path);
    }

    while(wait(&status) > 0);
    // copy each poster to dest dir
    child_e = fork();
    if(child_e == 0 && child_d == 0 && child_c > 0 && child_b > 0) {
        if(chdir("/home/anggito/shift2/drakor") < 0) perror("Error chdir to drakor!\n");
        DIR *dir_path;
        struct dirent *curr_file;

        dir_path = opendir(".");

        // dir listing to copy file to the suitable directory
        if(dir_path != NULL) {
            while(curr_file = readdir(dir_path)) {
                for(int i=0; i<filmIndex; i++) {
                    if(strstr(curr_file->d_name, posterInfo[i].title)) {
                        char catPath[80] = "/home/anggito/shift2/drakor/";
                        strcat(catPath, posterInfo[i].category);
                        strcat(catPath, "/");
                        strcat(catPath, posterInfo[i].title);
                        strcat(catPath, ".png");

                        if(fork() == 0) execl("/bin/cp", "cp_files", curr_file->d_name, catPath, NULL);
                    }
                }
            }
        }
        closedir(dir_path);
    }

    waitpid(child_d, NULL, 0);
    // make data.txt
    child_f = fork();
    if(child_f == 0 && child_e > 0 && child_d > 0) {

        if(chdir("/home/anggito/shift2/drakor") < 0) perror("Error chdir to drakor!\n");

        DIR *dir_path;
        struct dirent *curr_file;

        dir_path = opendir(".");

        if(dir_path != NULL) {
            while(curr_file = readdir(dir_path)) {
                if(strcmp(curr_file->d_name, ".") != 0 && strcmp(curr_file->d_name, "..") != 0) {
                    if(strstr(curr_file->d_name, ".png")) {
                        // do something
                    }
                    else {
                        // make data.txt
                        char dataTXTPath[100] = "/home/anggito/shift2/drakor/";
                        strcat(dataTXTPath, curr_file->d_name);
                        strcat(dataTXTPath, "/");
                        strcat(dataTXTPath, "data.txt");

                        if(fork() == 0) execl("/bin/touch", "touch_data.txt", dataTXTPath, NULL);
                    }
                }
            }
        }
        closedir(dir_path);
    }

    waitpid(child_f, NULL, 0);
    // listing data.txt
    child_g = fork();
    if(child_g == 0 && child_f > 0 && child_e > 0 && child_d == 0) {
        if(chdir("/home/anggito/shift2/drakor") < 0) perror("Error chdir to drakor!\n");

        // vars for dir listing
        DIR *dir_path_drakor;
        struct dirent *curr_file_drakor;

        // var for file txt editing
        FILE *data_txt;

        dir_path_drakor = opendir(".");

        if(dir_path_drakor != NULL) {
            while(curr_file_drakor = readdir(dir_path_drakor)) {
                if(strstr(curr_file_drakor->d_name, ".") == NULL) {
                    char catDirPath[150] = "/home/anggito/shift2/drakor/";
                    strcat(catDirPath, curr_file_drakor->d_name);

                    if(chdir(catDirPath) < 0) perror("Error chdir to cat");

                    // dir listing for each category
                    DIR *dir_path_cat;
                    struct dirent *curr_file_cat;

                    // PQ for listing
                    PQ catPQ;
                    pqueue_init(&catPQ);
            
                    dir_path_cat = opendir(".");

                    // file listing
                    strcat(catDirPath, "/data.txt");
                    data_txt = fopen(catDirPath, "w+");

                    char dataListing[1000] = "";
                    // title list
                    strcat(dataListing, "kategori : ");
                    strcat(dataListing, curr_file_drakor->d_name);

                    // find suitable film from struct
                    if(dir_path_cat != NULL) {
                        while(curr_file_cat = readdir(dir_path_cat)) {
                            if(strstr(curr_file_cat->d_name, ".png")) {
                                for(int i=0; i<filmIndex; i++) {
                                    if(strstr(curr_file_cat->d_name, posterInfo[i].title)) {
                                        pqueue_push(&catPQ, posterInfo[i].title, posterInfo[i].year);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    closedir(dir_path_cat);

                    while(!pqueue_isEmpty(&catPQ)) {
                        strcat(dataListing, "\n");
                        strcat(dataListing, "\n");

                        strcat(dataListing, "nama : ");
                        strcat(dataListing, catPQ.top->title);
                        strcat(dataListing, "\n");

                        strcat(dataListing, "rilis : tahun ");
                        char tempYear[30];
                        sprintf(tempYear, "%d", catPQ.top->year);
                        strcat(dataListing, tempYear);

                        pqueue_pop(&catPQ);
                    }

                    // append to data.txt
                    fprintf(data_txt, "%s\n", dataListing);
                }
            }
        }
        closedir(dir_path_drakor);
        fclose(data_txt);
    }
}
